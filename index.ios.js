import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  NavigatorIOS,
  View
} from 'react-native';

import Home from './app/home/home';

export default class tronome extends Component {
  render() {
    return (
        <NavigatorIOS initialRoute={{
                component: Home,
                title: 'GitHub reader',
            }}
            style={{flex: 1}}
          />
    );
  }
}

AppRegistry.registerComponent('tronome', () => tronome);
