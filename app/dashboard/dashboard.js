import React, { Component } from 'react';
import { StyleSheet, Text, View,
     Navigator, TextInput, Image, TouchableHighlight,
    ActivityIndicatorIOS } from 'react-native';

import Profile from './../profile/profile';
import Repos from './../repos/repos';
import api from './../utils/api';

export default class Dashboard extends Component {
  imageURL;
  constructor(props){
      super(props);
      this.imageURL = props.userInfo.avatar_url;
  }
  
  goToProfile(){
      this.props.navigator.push({
          component: Profile,
          title: 'Profile',
          passProps: {userInfo: this.props.userInfo}
      });
      
  }
  goToRepos(){
      
      api.getRepos(this.props.userInfo.login)
          .then((res) => {
              this.props.navigator.push({
                  component: Repos,
                  title: 'Repositories',
                  passProps: {
                      userInfo: this.props.userInfo,
                      repos: res
                  }
              });
          });
  }
  goToNotes(){
      console.log("Notes");
  }
  makeBackground(btn){
      var obj = {
          flexDirection: 'row',
          alignSelf: 'stretch',
          justifyContent: 'center',
          flex: 1
      }
      
      if(btn== 0){
          obj.backgroundColor = 'orange';
      } else if(btn==1){
          obj.backgroundColor = 'green';
      } else if(btn==2){
          obj.backgroundColor = 'rebeccapurple';
      }
      return obj;
  }
  
    render(){
        return(
            <View style={styles.container}>
                <Image
                  source={{uri:this.imageURL}}
                  style={styles.image}
                ></Image>
                <TouchableHighlight
                    style={this.makeBackground(0)}
                    onPress={this.goToProfile.bind(this)}
                    underlayColor='yellow'>
                    <Text style={styles.btnTxt}>Profile</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    style={this.makeBackground(1)}
                    onPress={this.goToRepos.bind(this)}
                    underlayColor='lime'>
                    <Text style={styles.btnTxt}>Repos</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    style={this.makeBackground(2)}
                    onPress={this.goToNotes.bind(this)}
                    underlayColor='hotpink'>
                    <Text style={styles.btnTxt}>Notes</Text>
                </TouchableHighlight>
                    
                
            </View>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        marginTop: 65,
        flex: 1
    },
    image: {
        height: 350,
    },
    btnTxt: {
        fontSize: 24,
        color: 'white',
        alignSelf: 'center'
    }
});
