import React, { Component } from 'react';
import { StyleSheet, Text, View, Navigator, TextInput, TouchableHighlight,
         ActivityIndicator } from 'react-native';

import api from './../utils/api';
import Dashboard from './../dashboard/dashboard';

export default class Home extends Component {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            isLoading: false,
            error: false
        };
    }//

    static get defaultProps(){
        return {
            title: 'Search for GitHub users'
        }
    }//

    handleChange(event){
        this.setState({
            username: event.nativeEvent.text
        });
    }//--handleChange()

    handleSubmit(event){
        this.setState({
            isLoading: true,
            error: false
        });
        api.getBio(this.state.username)
            .then((res) => {
                console.log("GOT res : ", res.message);
                if(res.message === 'Not Found'){
                    this.setState({
                        error: this.state.username + ' not found',
                        isLoading: false
                    })
                } else {
                    this.props.navigator.push({
                        title: res.name || "Select an option",
                        component: Dashboard,
                        passProps: {userInfo: res}
                    });
                    this.setState({
                        isLoading: false,
                        error: false,
                        username: ''
                    });
                }
            });

        console.log(" GOT username ", this.state.username);
    }//--handleSubmit()

    render(){
        var showErr = (
            this.state.error ? <Text style={styles.errTxt}> {this.state.error} </Text> : <View></View>
        );
        return (
            <View style={styles.confContainer}>
                <Text style={styles.title}>{this.props.label}</Text>
                <Text style={styles.title}>{this.props.title}</Text>
                <TextInput
                    style={styles.searchInput}
                    value={this.state.username}
                    onChange={this.handleChange.bind(this)}
                />
                <TouchableHighlight
                    style={styles.button}
                    value={this.state.username}
                    onPress={this.handleSubmit.bind(this)}
                    underlayColor='white'>
                    <Text style={styles.buttonTxt}>DOOKIE</Text>
                </TouchableHighlight>
                <ActivityIndicator
                    animating={this.state.isLoading}
                    color="white"
                    size="large"></ActivityIndicator>
                {showErr}
            </View>
        )
    }//--render()
}

var styles = StyleSheet.create({
    confContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'skyblue'
    },
    searchInput: {
        height: 50,
        marginLeft: 50,
        marginRight: 50,
        fontSize: 27,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 8,
        color: 'white'
    },
    title:{
        marginBottom: 20,
        fontSize: 27,
        textAlign: 'center',
        color: 'white'
    },
    button: {
        height: 45,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 8,
        marginLeft: 100,
        marginRight: 100,
        marginBottom: 10,
        marginTop: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    buttonTxt: {
        color: 'skyblue',
        fontSize: 30
    },
    errTxt: {
        color: 'white',
        fontSize: 30
    }
});
