import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

export default class Separator extends Component {
    render(){
        return (
            <View style={styles.separator}></View>
        )
    }
};

//-styles
var styles = StyleSheet.create({
    separator: {
        height: 1,
        backgroundColor: 'rebeccapurple',
        flex: 1,
        marginLeft: 25
    }
});