import React, { Component } from 'react';
import { StyleSheet, Text, View, Navigator, TextInput, TouchableHighlight,
    ActivityIndicator } from 'react-native';

import Badge from './../badge/badge';
import Separator from './../helpers/separator';

var styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    rowContainer: {
        flexDirection: 'column',
        flex: 1,
        padding: 10
    },
    name: {
        color: '#48BBEC',
        fontSize: 18,
        paddingBottom: 5
    },
    stars: {
        color: '#48BBEC',
        fontSize: 14,
        paddingBottom: 5
    },
    description: {
        fontSize: 14,
        paddingBottom: 5
    }
});

export default class Repos extends Component {
    openPage(url){
        console.log("URL ", url);    
    }
    
    render(){
        var repos = this.props.repos;
        var list = repos.map((item, index) => {
            //get the repo description as 'desc'
            var desc = repos[index].description ? <Text style={styles.description}>
                {repos[index].description} </Text> : <View />;
            
            return (
                <View key={index}>
                    <View style={styles.rowContainer}>
                        <TouchableHighlight
                            onPres={this.openPage.bind(this, repos[index].html_url)}
                            underlayColor='transparent'>
                            <Text style={styles.name}>{repos[index].name}</Text>
                            <Text style={styles.stats}> Stats: {repos[index].stargazers_count}</Text>
                            {desc}
                        </TouchableHighlight>
                    </View>
                </View>
            )
        })
        
        return (
            <ScrollView style={styles.container} >
                <Badge userInfo={this.props.userInfo} />
            </ScrollView>
        )
    }
}